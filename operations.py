'''
Funtions to implement basic arthematic operation
'''

from adders import fullAdder
from binaryOperations import twosComplement, binaryConvertion, decimalConvertion

def add(operand1,operand2):
	'''
	adds two decimal input numbers in binary with help of full adder 
	params: operand1(Int) and operand2(Int)
	return sumResult(Int)
	'''

	# converting operands into binary and getting signs
	binaryOperand1,operand1Sign = binaryConvertion(operand1)
	binaryOperand2,operand2Sign = binaryConvertion(operand2)
	resultSign = '0'
	# calculating sum and carry value by adding the bits in the
	# 	bits sequence
	sumValue = ''
	cummulativeCarryValue = 0
	for bitIndex in range(14,-1,-1):
		bitSum,cummulativeCarryValue = fullAdder(binaryOperand1[bitIndex], \
																			binaryOperand2[bitIndex], \
																			cummulativeCarryValue)
		sumValue = bitSum + sumValue
	else :
		carryOut = cummulativeCarryValue
	# calculate sign of result and check over flow
	if (operand1Sign == '1' or operand2Sign == '1'):
		resultSign,carryOut = fullAdder(operand1Sign,operand2Sign,carryOut)
	# convetion of resultant binary into decimal value
	binaryResult =  '0b'+sumValue
	decimalSumValue = eval(binaryResult)

	#	dealing with negitive value
	if (resultSign == '1'):
		decimalSumValue = twosComplement(decimalSumValue)
	calculatedSum = decimalSumValue if resultSign == '0' else -(decimalSumValue)
	# dealing with overflow occurances
	if (operand1Sign == operand2Sign and str(carryOut) != operand1Sign):
		return ("Over flow occured when calculating.......")

	return (calculatedSum)

def sub(operand1,operand2):
	'''
	substracts two operands by binary calculation
	params: operand1(Int) and operand2(Int)
	return subResult(Int)
	'''

	return (add(operand1,-(operand2)))

def mul(operand1,operand2):
	'''
	multiplies two operands in binary with help of full adder and addition
	 function
	params: operand1(Int) and operand2(Int)
	return mulResult(Int)
	'''

	# validating user input parameters
	#		limited to 2**8-1 and 2**8-1
	#		overflow occurs when both operands bit combined is greater than 16
	#		but their will be 2 more characters from each bin return ('0bxxx')
	#		so 20(16+4)
	if (len(bin(abs(operand1)))+len(bin(abs(operand2)))>20):
		return ("Over flow occured when calculating.......")
	#	creating a variable to store cummulative multiplication result
	# 	(cummulativeOutput)
	cummulativeOutput = 0
	# get signs of operands
	operand1Sign = binaryConvertion(operand1)[1]
	operand2Sign = binaryConvertion(operand2)[1]
	#	get the binary value of operand1
	binaryOperand1 = binaryConvertion(abs(operand1))[0]
	# get absolute value of operand2
	absoluteOperand2 = abs(operand2)
	# iterate through the bits of binaryOperand1 from right to left
	for bitIndex in range(14,-1,-1):
	#	when a bit in binaryOperand1 is '1' left shift the multiplicand(operand2)
	#		then add it to the cumulativeOutput 
		if (binaryOperand1[bitIndex] == '1'):
			cummulativeOperand1 = absoluteOperand2<<(14-bitIndex)
			cummulativeOutput = add(cummulativeOutput,cummulativeOperand1)
	# dealing with overflow condition
		if (cummulativeOutput == "Over flow occured when calculating......."):
			print("try multiplication of lower numbers")

			return cummulativeOutput

	binaryMultipliedValue = binaryConvertion(cummulativeOutput)[0]
	# convetion of resultant binary into decimal value
	binaryResult =  '0b' + binaryMultipliedValue
	decimalMulValue = eval(binaryResult)

	# calculating sign of resultant
	resultSign = int(operand1Sign) ^ int(operand2Sign)
	calculatedMultiplication = decimalMulValue if resultSign == 0 else -(decimalMulValue)

	return 	(calculatedMultiplication)

def div(dividend,divisor):
	'''
	params: dividend(Int) and divisor(Int)
	return decimalQuotient(Int or Str('Inf')), decimalReminder(Int)
	divides dividend by divisor using long int division
	and returns Quotient and Reminder
	'''

	# checks whether divisor is zero to avoid ZeroDividionError
	if (divisor == 0):
		#	return quotient as infinite
		return ('Inf')
	# converts input dividend to binary and finds sign of both dividend 
	# 	and divisor
	dividentSign = binaryConvertion(dividend)[1]
	divisorSign = binaryConvertion(divisor)[1]
	binarydivident = binaryConvertion(abs(dividend))[0]
	# creating cummulativeDividend and quotient
	cummulativeDividend = ''
	quotient = ''
	absDivisor = abs(divisor)
	# calculating the result using long int division
	for bitIndex in range(15):
	#	adding bits to cummulativeDividend if it is divisble by divisor 
		cummulativeDividend += binarydivident[bitIndex]

	# if the cumulativeDividend is greater than division perform division
		if (decimalConvertion(cummulativeDividend) >= absDivisor):
	#		substract the dividend and cumulativeDividend
			cummulativeDividend = sub(decimalConvertion(cummulativeDividend),absDivisor)
	#		gets the bit stream of cumulativeDividend
			cummulativeDividend = binaryConvertion(cummulativeDividend)[0]
	#		update quotient by one			
			quotient += '1'			
	#	else update quotient by zero 
		else:			
			quotient += '0'
	#	assigning cummulativeDividend to binaryReminder
	else:
		binaryReminder = cummulativeDividend

	# converting obtain values into decimal representation
	decimalQuotient = decimalConvertion(quotient)
	decimalReminder = decimalConvertion(binaryReminder)

	# calculate sign of Output
	resultSign = int(dividentSign) ^ int(divisorSign)
	calculatedDivision = decimalQuotient if resultSign == 0 else -(decimalQuotient)

	# returning calculatedDivision
	return (calculatedDivision)

