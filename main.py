'''
A script to do binary Addition, Substraction, Multiplication and division for
	input decimal numbers
@author Rijina
'''

from getInput import getInt,getOP
from operations import add, sub, mul, div

def main():
	"""
	Main program
	"""

	# prompt user for input operators and operand
	operand1 = getInt("Enter Operand1 :")
	operator = getOP("What operation do you want to perform :")
	operand2 = getInt("Enter Operand2 :")
	# overflow message 
	overflowMessage = "Over flow occured when calculating......."
	#	Perform operation as per operator
	if (operator in ['+','add','addition']):
		calculatedValue = add(operand1, operand2)
	elif (operator in ['-','sub','substraction']):
		calculatedValue = sub(operand1, operand2)
	elif (operator in ['*','mul','multiply']):
		calculatedValue = mul(operand1,operand2)
	elif (operator in  ['/','div','divide']):
		calculatedValue = div(operand1, operand2)
		#dealing with zero division error
		if (calculatedValue == 'Inf'):
			print(">Dividing by zero is not possible")
	elif (operator == 0):
		print("Exiting...............")
	
	if (calculatedValue == overflowMessage):
		print(overflowMessage)
	#check for infinite output
	elif (calculatedValue != 'Inf'):
		print("The Calculated result is : ",calculatedValue)

		
if __name__ == '__main__':
	main()
