'''
Implementation of full adder funtion
'''

def fullAdder(bit1Value,bit2Value,carryIn):
	'''
	Implementation of full adder Truth Table
	params: bit1Value(Str), bit2Value(Str), carryIn(Int)
	returns: sumValue(Str), carryOut(Int)
	'''
	if (bit1Value == '1' and bit2Value == '1'):
		if (carryIn == 0):
			sumValue = '0'
			carryOut = 1
		else:
			sumValue = '1'
			carryOut = 1
	elif (bit1Value == '0' and bit2Value == '1'):
		if (carryIn == 0):
			sumValue = '1'
			carryOut = 0
		else:
			sumValue = '0'
			carryOut = 1
	elif (bit1Value == '0' and bit2Value == '0'):
		if (carryIn == 0):
			sumValue = '0'
			carryOut = 0
		else:
			sumValue = '1'
			carryOut = 0
	elif (bit1Value == '1' and bit2Value == '0'):
		if (carryIn == 0):
			sumValue = '1'
			carryOut = 0
		else:
			sumValue = '0'
			carryOut = 1
	return (sumValue,carryOut)
