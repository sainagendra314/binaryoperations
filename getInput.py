'''
An Implementation to get user input and varify if it satisfies
required type
'''

from validators import validateUserInputRange
from isOperator import isOperator


def getInt(promptMessage, errorMessage= '', retries = "#TODO"):
	'''
	prompt user for input and checks it and return userValue 
		accept integers only of bit length 16
	Parameters
	----------

	promptMessage : str
		text message that should be shown to the user to prompt input
	errorMessage : str (optional)
		text that should show when error is raised 
	retries : int
		number of tries before running out of error time

	Returns
	-------

	userValue : Int
		returns userValue when it satisfies the type
	'''

	while(True):
		try:
			userValue = int(input(promptMessage))
			#		check if in the range of the 16bit Signed representation
			if (validateUserInputRange(userValue) != 'pass'):
				raise ValueError
			return userValue
		except ValueError:
			print(f"Enter Integer value only in defined range only(Between {-2**15}" 
				f" and {-2**15-1})")
	
def getOP(promptMessage,errorMessage = ''):
	'''
	prompt user for input and checks it and return userValue 
		accept only operand types (+, -, *, /) 
	Parameters
	----------

	promptMessage : str
		text message that should be shown to the user to prompt input
	errorMessage : str (optional)
		text that should show when error is raised 

	Returns
	-------

	userValue : type of op(+, -, *, /)
		returns userValue when it satisfies the type

	Raises:

	'''
	while(True):
		try: 
			userValue = input(promptMessage)
			isOperator(userValue)
			return userValue
		except ValueError:
			print("Select a Valid operation (+, -, *, /)")
