'''
Funtion to check if input param is in given bit range
'''


def validateUserInputRange(param,binarySize = 16):
	'''
	validtion of user input between range of -2**15 to 2**15-1

	params : 
		param(Int): Value which the range is to be checked
		binarySize(Int): (optional) size of Int

	returns:
		tell if param in range 
	'''

	if (param > (2**(binarySize-1) - 1) or param < (-2**(binarySize-1))):
		return (" The given operand is not in operatable range")
	else:
		return ("pass")
