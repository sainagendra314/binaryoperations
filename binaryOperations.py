'''
Implementation of twosComplement, binaryConvertion, decimalConvertion
'''

def twosComplement(bitSequence,binarySize = 16):
	'''
	params: bitSequence, binarySize
	return: 2's complent of the input bitSequence
	bitSequence is converted to twos complement of size of representation
	binarySize is to reduce by 1 to convert 15 bit(exclude 1 sign bit) and
		get 1's complement
	and then add 1 to convert to 2's complement 
	'''

	# calculates ones complement
	onesCompliment=(bitSequence ^ (2 ** (binarySize-1) - 1))
	# returns twos complement
	return (onesCompliment + 1) 


def toBinary(decimalNumber):
	'''
	This function takes decimal number as input and 
	returns binaryvalue in form of string
	param:decimalNumber(Int) in decimal format
	return cumulativeBinary(Str) in string format
	''' 
	cumulativeBinary=''
	if decimalNumber==0:
		cumulativeBinary=cumulativeBinary + '0'
		return (cumulativeBinary)
	# if decimalNumber is not equal to 1
	else:
		while decimalNumber!=1:
			# calculating remainder and updating decimalNumber 
			remainder = decimalNumber%2
			decimalNumber = decimalNumber//2
			# cumulatively adding the remainder obtained
			cumulativeBinary = cumulativeBinary + str(remainder)
		# adding the final quotient to remainder    
		cumulativeBinary = cumulativeBinary+str(decimalNumber)
		binaryResult = cumulativeBinary[::-1]
		return(binaryResult)       
 
def  toDecimal(binaryString):
    '''
    This function takes binary string as an input and
    returns decimal value 
    param: binaryString(Str) in string format
    return decimalValue(Int) in decimal format
    '''

    reverseDecimal = binaryString[::-1]
    # iterating through the elements, multiplying each element with
    #   the 2 power of index and cumulatively adding the result obtained
    decimalValue = sum([int(bit)*(2**i) for i,bit in enumerate(reverseDecimal)])
    return(decimalValue)

def binaryConvertion(decimalNumber):
	'''
	param: decimalNumber(Int)
	returns tuple containing 15bit binary value(Str) and sign bit(Str) of 
		decimalNumber
	'''

	# get the sign and do 2's complement if either of two operands is less
	#  than zero
	signBit = '1' if decimalNumber<0 else '0'
	if (signBit == '1'):
		decimalNumber = twosComplement(-decimalNumber)

	# converts operand1 and operand2 to binary and extracts binary
	# 	sequence as string
	binaryConvResult = toBinary(decimalNumber)
	#	padding zeros to convert binaryConvResult into 15 bit representation
	binaryConvResult = '0'*(15 - len(binaryConvResult)) + binaryConvResult
	return (binaryConvResult,signBit)

def decimalConvertion(binaryString):
	'''
	param: binaryString(Str) in decimal format
	returns decimal value(Int) of binaryString
	'''

	return (toDecimal(binaryString))
