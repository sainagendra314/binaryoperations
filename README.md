# Custom Math Library

Addition, Substraction, Multiplication and division for input decimal numbers (Using Binary Calculations)

## Getting Started

The arthematic funtion are implemented in operations.py file

getInput validates user input

Just run main funtion and enter operands and operator 

## Authors

* **Ganji Naga Sai Nagendra** - (https://gilab.com/sainagendra)

* **Myadam Rithvik** - (https://gitlab.com/rithikmydam)

* **Yengandula Jalaja Sree** 

See also the list of [contributors](https://gitlab.com/sainagendra314/binaryoperations/-/graphs/master) who participated in this project.

## License

This project is licensed under the MIT License
