'''
Implementation to check if param is an Operator type
'''

def isOperator(param):
	"""
	check wether param is a operand or Not

	Parameters
	----------
	param : str
		A string value which will be verified if operand

	Returns
	-------
	Does not return any value

	Raises
	------
	ValueError:
	 if param is not a operator
	"""

	if (param in ['+', '-', '*', '/']):
		pass
	elif (param.lower() in ['add','sub','mul','div']):
		pass
	elif (param.lower() in ['addition','subtraction','multiply','divide']):
		pass
	else:
		raise ValueError('Enter valid Operator only')
